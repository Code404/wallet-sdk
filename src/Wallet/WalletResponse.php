<?php
namespace Wallet;

class WalletResponse
{
    private $response;

    public function __construct($response)
    {
        $this->response = $response;
        if(!in_array($this->response->getStatusCode(), [200, 201, 202])) {
            try {
                $json = $this->response->json();
            }catch (\Exception $e){
                //
                //Do nothing, for now.
                //
                $json['message'] = '';
            }

            $message = 'Error with the wallet request' . (empty($json['message']) ? '' : ': ' . $json['message']);
            throw new WalletException($message, $this->response);
        }
    }

    public function get()
    {
        return $this->response->json()['data'];
    }
}
