<?php

namespace Wallet\Traits;

trait TransactionTrait 
{
    
    use TransactionMockTrait;
    
    /**
     * Get a user wallet transaction list
     * 
     * @param int $userId
     * @param int $page
     * @param int $count
     * @param string $status
     * @param string $sortField
     * @param string $sortDirection
     * @return mixed
     */
    public function getTransactions($userId, $page = 1, $count = 10, $status = 'ACTIVE', $sortField = 'id', $sortDirection = 'DESC')
    {
        if ($this->mocks) {
            return $this->getTransactionsMock($userId, $page, $count, $status, $sortField, $sortDirection);
        }
        
        $params = [
            'page' => $page,
            'count' => $count,
            'status' => $status,
            'order' => $sortField,
            'sort' => $sortDirection,
        ];

        return $this->__doGet('/api/wallet/' . $userId . '/transactions', $params);
    }

    /**
     * Get a transaction
     * 
     * @param int $transactionId
     * @return mixed
     */
    public function getTransaction($transactionId)
    {
        if ($this->mocks) {
            return $this->getTransactionMock($transactionId);
        }
        
        return $this->__doGet('/api/transaction/' . $transactionId);
    }

    /**
     * Creates a new confirmation pending transaction
     * 
     * @param int $userId
     * @param string $psp
     * @param decimal $amount
     * @param decimal $vat
     * @param array $products
     * @return mixed
     */
    public function initTransaction($userId, $psp = 'PAYPAL', $amount, $vat = 0, array $products = [])
    {
        if ($this->mocks) {
            return $this->initTransactionMock($userId, $psp, $amount, $vat, $products);
        }
        
        $postBody = [
            'user_id' => $userId,
            'psp' => $psp,
            'amount' => $amount,
            'vat' => $vat,
        ];
        
        if (!empty($products)) {
            $postBody['products'] = $products;
        }

        return $this->__doPost('/api/transaction', $postBody);
    }
    
    /**
     * Creates a new confirmation pending transaction, and pay products with this
     * 
     * @param int $userId
     * @param string $psp
     * @param decimal $amount
     * @param decimal $vat
     * @param array $products
     * @return mixed
     */
    public function initTransactionWithProducts($userId, $psp = 'PAYPAL', $amount, $vat = 0, array $products = [])
    {
        if ($this->mocks) {
            return $this->initTransactionWithProductsMock($userId, $psp, $amount, $vat, $products);
        }
        
        return $this->initTransaction($userId, $psp, $amount, $vat, $products);
    }
    
    
    /**
     * Confirm a pending transaction sending a payment notification
     * 
     * @param int $transactionId
     * @param array $ipn
     * @return mixed
     */
    public function confirmTransaction($transactionId, $ipn)
    {
        if ($this->mocks) {
            return $this->confirmTransactionMock($transactionId, $ipn);
        }
        
        $postBody = [
            'ipn' => $ipn
        ];
        return $this->__doPut('/api/transaction/' . $transactionId, $postBody);
    }

    /**
     * Creates a new confirmation pending refund
     * 
     * @param int $transactionId
     * @param decimal $amount
     * @return mixed
     */
    public function initRefund($transactionId, $amount = null)
    {
        if ($this->mocks) {
            return $this->initRefundMock($transactionId, $amount);
        }
        
        $postBody = ['amount' => $amount];
        return $this->__doPost('/api/transaction/' . $transactionId . '/refund', $postBody);
    }

    /**
     * Confirm a pending refund
     * 
     * @param int $transactionId
     * @param string $txnId
     * @param string $status
     * @return mixed
     */
    public function confirmRefund($transactionId, $txnId, $status = 'Refunded')
    {
        if ($this->mocks) {
            return $this->confirmRefundMock($transactionId, $txnId, $status);
        }
        
        $postBody = [
            'status' => $status,
            'txn_id' => $txnId,
        ];

        return $this->__doPut('/api/transaction/' . $transactionId . '/refund', $postBody);
    }
    
    /**
     * If your application received an unexpected IPN, like a Reversed transaction, 
     * you can send it directly to the wallet.
     * 
     * @param array $ipn
     * @param string $psp
     * @return mixed
     */
    public function sendIpn(array $ipn, $psp = 'PAYPAL')
    {
        if ($this->mocks) {
            return $this->sendIpnMock($ipn, $psp);
        }
        
        $postBody = [
            'psp' => $psp,
            'ipn' => $ipn
        ];
        
        return $this->__doPost('/api/ipn', $postBody);
    }
}
