<?php

namespace Wallet\Traits;

trait WalletMockTrait 
{
    /**
     * Creates a wallet for the provided user.
     * If an email is sent, it will be stored as verified merchant email.
     *
     * @param int $userId
     * @param string $email
     * @param array billingData
     * @return mixed
     */
    protected function createMock($userId, $email = null, array $billingData = [])
    {
        $response = [
            "user_id" => $userId,
            "amount" => "1000000.00",
        ];
        
        if (!is_null($email)) {
            $response['email'] = [[
                "email" => $email,
                "status" => "ACTIVE"
            ]];
        }
        
        if (!empty($billingData)) {
            $response['billing_data'] = $billingData;
        }
        
        return $response;
    }

    /**
     * Gets an existing user wallet
     *
     * @param int $userId
     * @return mixed
     */
    protected function getMock($userId)
    {
        return [
            "user_id" => $userId,
            "amount" => "1000000.00",
            "email" => [[
                "email" => "user@wallet.com",
                "status" => "ACTIVE"
            ]],
            "billing_data" => [
                "full_name" => "John Doe",
                "address_1" => "Somestreet 123",
                "address_2" => null,
                "city" => "Barcelona",
                "county" => "Barcelona",
                "zip_code" => "08023",
                "company_name" => null,
                "vat" => null,
                "vat_validate" => null,
                "country_code" => "ES"
            ]
        ];
    }

    /**
     * Gets and creates the wallet if needed
     *
     * @param int $userId
     * @param string $email
     * @param array $billingData
     * @return mixed
     */
    protected function getOrCreateMock($userId, $email = null, array $billingData = [])
    {
        return $this->createMock($userId, $email, $billingData);
    }
    
    /**
     * Activates a pending email account
     * 
     * @param int $userId
     * @param string $email
     * @param boolean $products
     * @return mixed
     */
    protected function confirmEmailAccountMock($userId, $email, $products = false)
    {
        return  [
            'email' => $email, 
            'status' => $status, 
        ];
    }
    
    /**
     * Add a new email account
     * 
     * @param int $userId
     * @param string $email
     * @param string $status = 'ACTIVE'
     * @return mixed
     */
    protected function addEmailAccountMock($userId, $email, $status = 'ACTIVE')
    {
        return  [
            'email' => $email, 
            'status' => $status, 
        ];
    }
    
    /**
     * Set the billing data for the wallet ( see http://wallet.code404.es/docs/api/wallet#post-billing-data )
     * 
     * @param int $userId
     * @param array $billingData
     * @return mixed
     */
    protected function setBillingDataMock($userId, array $billingData)
    {
        return $billingData;
    }
}
