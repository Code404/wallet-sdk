<?php

namespace Wallet\Traits;

trait ProductTrait 
{
    use ProductMockTrait;
    
    /**
     * Pay a product using wallet funds
     * 
     * @param int $userId
     * @param string $remoteId
     * @param decimal $amount
     * @return mixed
     */
    public function payProduct($userId, $remoteId, $amount)
    {
        if ($this->mocks) {
            return $this->payProductMock($userId, $remoteId, $amount);
        }
        
        $postBody = [
            'user_id'   => $userId,
            'remote_id' => $remoteId,
            'amount'    => $amount
        ];
        
        return $this->__doPost('/api/product', $postBody);
    }
    
    /**
     * Get a paid product
     * 
     * @param string $remoteId
     * @return mixed
     */
    public function getProduct($remoteId)
    {
        if ($this->mocks) {
            return $this->getProductMock($remoteId);
        }
        
        return $this->__doGet('/api/product/' . $remoteId);
    }

    /**
     * Cancel the payment for a given product
     * 
     * @param string $remoteId
     * @param decimal $amount = null
     * @return mixed
     */
    public function cancelProduct($remoteId, $amount = null)
    {
        if ($this->mocks) {
            return $this->cancelProductMock($remoteId, $amount);
        }
        
        $postBody = [];
        if (isset($amount)) {
            $postBody['refunded_amount'] = $amount;
        }

        return $this->__doPut('/api/product/' . $remoteId . '/cancel', $postBody);
    }
}
