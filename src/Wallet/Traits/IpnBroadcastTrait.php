<?php

namespace Wallet\Traits;

trait IpnBroadcastTrait 
{
    /**
     * Creates a new endpoint for the IPN broadcasting.
     * 
     * @param string $url
     * @param string $psp | optional
     * @return mixed
     */
    public function createIpnEndpoint($url, $psp = null)
    {
        $postBody = ['url' => $url];
        
        if (!empty($psp)) {
            $postBody['psp'] = $psp;
        }
        
        return $this->__doPost('/api/ipnbroadcast/ipnendpoint', $postBody);
    }
    
    /**
     * Update an existing endpoint for the IPN broadcasting.
     * 
     * @param int $endpointId
     * @param string $url
     * @param string $psp
     * @return mixed
     */
    public function updateIpnEndpoint($endpointId, $url, $psp = null)
    {
        $postBody = ['url' => $url];
        
        if (!empty($psp)) {
            $postBody['psp'] = $psp;
        }
        
        return $this->__doPut('/api/ipnbroadcast/ipnendpoint/' . $endpointId, $postBody);
    }
    
    /**
     * Delete an existing endpoint for the IPN broadcasting
     * 
     * @param int $endpointId
     * @return mixed
     */
    public function deleteIpnEndpoint($endpointId)
    {
        return $this->__doDelete('/api/ipnbroadcast/ipnendpoint/' . $endpointId);
    }
}
