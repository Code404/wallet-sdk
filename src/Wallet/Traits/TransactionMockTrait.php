<?php

namespace Wallet\Traits;

trait TransactionMockTrait 
{
    
    /**
     * Get a user wallet transaction list
     * 
     * @param int $userId
     * @param int $page
     * @param int $count
     * @param string $status
     * @param string $sortField
     * @param string $sortDirection
     * @return mixed
     */
    protected function getTransactionsMock($userId, $page = 1, $count = 10, $status = 'ACTIVE', $sortField = 'id', $sortDirection = 'DESC')
    {
        $transactionRemaining = $this->getTransactionMock(2);
        $transactionRemaining['remaining'] = 24;
        unset($transactionRemaining['products']);
        unset($transactionRemaining['operations']);
        
        return [
            "transactions" => [
                $this->getTransactionMock(1),
                $transactionRemaining,
            ],
            "pager" => [
                "total" => 2,
                "pages" => 1
            ]
        ];
    }

    /**
     * Get a transaction
     * 
     * @param int $transactionId
     * @return mixed
     */
    protected function getTransactionMock($transactionId)
    {
        $now = new \DateTime();
        
        return [
            "id" => $transactionId,
            "wallet_user_id" => 3,
            "psp" => "PAYZA",
            "txn_id" => "55f6a7e41df77",
            "payer_id" => null,
            "payer_status" => null,
            "type" => "IN",
            "amount" => "24.00",
            "vat" => "5.04",
            "remaining" => "0.00",
            "status" => "ACTIVE",
            "created_at" => $now->format('Y-m-d H:i:s'),
            "updated_at" => $now->format('Y-m-d H:i:s'),
            "payer_email" => "user@wallet.es",
            "products" => [[
                "id" => 4,
                "remote_id" => "4",
                "amount" => "24.00",
                "refunded_amount" => "0.00",
                "extra_data" => null,
                "spent" => "24.00"
            ]],
            "operations" => [[
                "status" => "BUY_PRODUCT",
                "product" => [
                    "id" => 4,
                    "remote_id" => "4",
                    "amount" => "24.00",
                    "refunded_amount" => "0.00",
                    "extra_data" => null
                ],
                "amount" => "-24.00",
                "datetime" => $now->format('Y-m-d H:i:s')
            ]]
        ];
    }

    /**
     * Creates a new confirmation pending transaction
     * 
     * @param int $userId
     * @param string $psp
     * @param decimal $amount
     * @param decimal $vat
     * @param array $products
     * @return mixed
     */
    protected function initTransactionMock($userId, $psp = 'PAYPAL', $amount, $vat = 0, array $products = [])
    {
        $now = new \DateTime();
        
        $response = [
            "id" => 17,
            "wallet_user_id" => $userId,
            "psp" => $psp,
            "txn_id" => null,
            "payer_id" => null,
            "payer_status" => null,
            "type" => "IN",
            "amount" => $amount,
            "vat" => $vat,
            "remaining" => "0",
            "status" => "PENDING",
            "created_at" => $now->format('Y-m-d H:i:s'),
            "updated_at" => $now->format('Y-m-d H:i:s')
        ];
        
        $pid = 5;
        if (!empty($products)) {
            $response['products'] = [];
            foreach ($products as $p) {
                $response['products'][] = [
                    "id" => $pid,
                    "remote_id" => $p['remote_id'],
                    "amount" => $p['amount'],
                    "refunded_amount" => 0,
                    "extra_data" => null,
                    "spent" => 0
                ];
                $pid++;
            }
        }
        
        return $response;
    }
    
    /**
     * Creates a new confirmation pending transaction, and pay products with this
     * 
     * @param int $userId
     * @param string $psp
     * @param decimal $amount
     * @param decimal $vat
     * @param array $products
     * @return mixed
     */
    protected function initTransactionWithProductsMock($userId, $psp = 'PAYPAL', $amount, $vat = 0, array $products = [])
    {
        return $this->initTransactionMock($userId, $psp, $amount, $vat, $products);
    }
    
    
    /**
     * Confirm a pending transaction sending a payment notification
     * 
     * @param int $transactionId
     * @param array $ipn
     * @return mixed
     */
    protected function confirmTransactionMock($transactionId, $ipn)
    {
        $now = new \DateTime();
        
        return [
            "id" => $transactionId,
            "wallet_user_id" => 1,
            "psp" => 'PAYPAL',
            "txn_id" => "pp1001",
            "payer_id" => "123",
            "payer_status" => "verified",
            "type" => "IN",
            "amount" => 20,
            "vat" => 0,
            "remaining" => 25,
            "status" => "ACTIVE",
            "payer_email" => "user@wallet.com",
            "created_at" => $now->format('Y-m-d H:i:s'),
            "updated_at" => $now->format('Y-m-d H:i:s')
        ];
    }

    /**
     * Creates a new confirmation pending refund
     * 
     * @param int $transactionId
     * @param decimal $amount
     * @return mixed
     */
    protected function initRefundMock($transactionId, $amount = null)
    {
        $now = new \DateTime();
        
        return [
            "id" => $transactionId,
            "wallet_user_id" => 1,
            "psp" => "PAYPAL",
            "txn_id" => null,
            "payer_id" => "123",
            "payer_status" => "verified",
            "type" => "OUT",
            "amount" => $amount,
            "vat" => "0",
            "remaining" => "0",
            "status" => "REFUND_PENDING",
            "created_at" => $now->format('Y-m-d H:i:s'),
            "updated_at" => $now->format('Y-m-d H:i:s'),
            "payer_email" => "user@wallet.com",
            "parent_id" => 18,
            "refund_type" => "FULL",
            "parent_txn_id" => "pp1000"
        ];
    }

    /**
     * Confirm a pending refund
     * 
     * @param int $transactionId
     * @param string $txnId
     * @param string $status
     * @return mixed
     */
    protected function confirmRefundMock($transactionId, $txnId, $status = 'Refunded')
    {
        $now = new \DateTime();
        
        return [
            "id" => $transactionId,
            "wallet_user_id" => 1,
            "psp" => "PAYPAL",
            "txn_id" => $txnId,
            "payer_id" => "123",
            "payer_status" => "verified",
            "type" => "OUT",
            "amount" => 24,
            "vat" => "0",
            "remaining" => "0",
            "status" => "REFUNDED",
            "created_at" => $now->format('Y-m-d H:i:s'),
            "updated_at" => $now->format('Y-m-d H:i:s'),
            "payer_email" => "user@wallet.com",
            "parent_id" => 18,
            "refund_type" => "FULL",
            "parent_txn_id" => "pp1000"
        ];
    }
    
    /**
     * If your application received an unexpected IPN, like a Reversed transaction, 
     * you can send it directly to the wallet.
     * TODO: pending
     * 
     * @param array $ipn
     * @param string $psp
     * @return mixed
     */
    protected function sendIpnMock(array $ipn, $psp = 'PAYPAL')
    {
        return [];
    }
}
