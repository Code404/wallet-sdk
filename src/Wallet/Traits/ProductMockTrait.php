<?php

namespace Wallet\Traits;

trait ProductMockTrait 
{
    /**
     * Pay a product using wallet funds
     * 
     * @param int $userId
     * @param string $remoteId
     * @param decimal $amount
     * @return mixed
     */
    protected function payProductMock($userId, $remoteId, $amount)
    {
        return [
            'user_id'   => $userId,
            'remote_id' => $remoteId,
            'amount'    => $amount,
            "refunded_amount" => 0,
            "extra_data" => null
        ];
    }
    
    /**
     * Get a paid product
     * 
     * @param string $remoteId
     * @return mixed
     */
    protected function getProductMock($remoteId)
    {
        return [
            "id" => 5,
            "remote_id" => $remoteId,
            "amount" => "24.00",
            "refunded_amount" => "0.00",
            "extra_data" => null,
            "transactions" => [4587]
        ];
    }

    /**
     * Cancel the payment for a given product
     * 
     * @param string $remoteId
     * @param decimal $amount = null
     * @return mixed
     */
    protected function cancelProductMock($remoteId, $amount = null)
    {
        $amount = is_null($amount) ? '24.00' : $amount;
        
        return [
            "id" => 5,
            "remote_id" => $remoteId,
            "amount" => $amount,
            "refunded_amount" => $amount,
            "extra_data" => null,
            "transactions" => [4587]
        ];
    }
}
