<?php

namespace Wallet\Traits;

trait WalletTrait 
{
    use WalletMockTrait;
    
    /**
     * Creates a wallet for the provided user.
     * If an email is sent, it will be stored as verified merchant email.
     *
     * @param int $userId
     * @param string $email
     * @param array billingData
     * @return mixed
     */
    public function create($userId, $email = null, array $billingData = [])
    {
        if ($this->mocks) {
            return $this->createMock($userId, $email, $billingData);
        }
        
        $postBody = ['user_id' => $userId];

        if (!empty($email)) {
            $postBody['email'] = [
                'email' => $email,
                'status' => 'ACTIVE',
            ];
        }
        
        if (!empty($billingData)) {
            $postBody['bolling_data'] = $billingData;
        }

        return $this->__doPost('/api/wallet', $postBody);
    }

    /**
     * Gets an existing user wallet
     *
     * @param int $userId
     * @return mixed
     */
    public function get($userId)
    {
        if ($this->mocks) {
            return $this->getMock($userId);
        }
        
        return $this->__doGet('/api/wallet/' . $userId);
    }

    /**
     * Gets and creates the wallet if needed
     *
     * @param int $userId
     * @param string $email
     * @param array $billingData
     * @return mixed
     */
    public function getOrCreate($userId, $email = null, array $billingData = [])
    {
        if ($this->mocks) {
            return $this->getOrCreateMock($userId, $email, $billingData);
        }

        try{
            $wallet = $this->get($userId);
        } catch (\Wallet\WalletException $e) {
            $errors = $e->getErrors();
            if($errors['statusCode'] == 404){
                $wallet = $this->create($userId, $email, $billingData);
            }
        }

        return $wallet;
    }
    
    /**
     * Activates a pending email account
     * 
     * @param int $userId
     * @param string $email
     * @param boolean $products
     * @return mixed
     */
    public function confirmEmailAccount($userId, $email, $products = false)
    {
        if ($this->mocks) {
            return $this->confirmEmailAccountMock($userId, $email, $products);
        }
        
        return $this->__doPut('/api/wallet/' . $userId . '/email', [
            'email' => $email, 
            'status' => 'ACTIVE', 
            'products' => (int)$products
        ]);
    }
    
    /**
     * Add a new email account
     * 
     * @param int $userId
     * @param string $email
     * @param string $status = 'ACTIVE'
     * @return mixed
     */
    public function addEmailAccount($userId, $email, $status = 'ACTIVE')
    {
        if ($this->mocks) {
            return $this->addEmailAccountMock($userId, $email, $status);
        }
        
        return $this->__doPost('/api/wallet/' . $userId . '/email', [
            'email' => $email, 
            'status' => $status, 
        ]);
    }
    
    /**
     * Set the billing data for the wallet ( see http://wallet.code404.es/docs/api/wallet#post-billing-data )
     * 
     * @param int $userId
     * @param array $billingData
     * @return mixed
     */
    public function setBillingData($userId, array $billingData)
    {
        if ($this->mocks) {
            return $this->setBillingDataMock($userId, $billingData);
        }
        
        return $this->__doPost('/api/wallet/' . $userId . '/billingdata', $billingData);
    }
}
