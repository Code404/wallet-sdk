<?php
namespace Wallet;

class WalletException extends \Exception
{
	private $response;

	public function __construct($message = "", $response, $code = 0, \Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);

		$this->response = $response;
	}

	public function getErrors()
    {
		$errors = [
			'statusCode' => $this->response->getStatusCode(),
			'reasonPhrase' => $this->response->getReasonPhrase(),
			'headers' => $this->response->getHeaders()->toArray(),
			'body' => $this->response,
		];

		return $errors;
	}
}
?>
