<?php

namespace Wallet;

class Wallet
{
    use Traits\WalletTrait;
    use Traits\TransactionTrait;
    use Traits\ProductTrait;
    use Traits\IpnBroadcastTrait;
    
    const VERSION = '1.0';

    protected $options = [
        'turn_off_ssl_verification' => false,
        'protocol' => 'http',
        'host' => 'wallet.code404.es',
        'port' => '80',
    ];
    
    /**
     *
     * @var \Guzzle\Http\Client
     */
    protected $client;
    
    protected $mocks;

    public $version = self::VERSION;
    
    public $apiAccessToken;
    
    public $url;

    public function __construct($apiAccessToken, $options = [])
    {
        // Check if given an access token
        if (!empty($apiAccessToken)) {
            // Username and password
            $this->apiAccessToken  = $apiAccessToken;
        } else {
            throw new \Exception('Need a an access token to use the API!');
        }

        $this->mocks = empty($options['mocks']) ? false : $options['mocks'];
        if ($this->mocks) {
            return;
        }
        
        $this->options['turn_off_ssl_verification'] = !empty($this->options['turn_off_ssl_verification']);

        if (!empty($options['protocol'])) {
            $this->options['protocol'] = $options['protocol'];
        }

        if (!empty($options['host'])) {
            $this->options['host'] = $options['host'];
        }

        if (!empty($options['port'])) {
            $this->options['port'] = $options['port'];
        }
        
        $this->url = $this->options['protocol'] . '://' . $this->options['host'] . ($this->options['port'] ? ':' . $this->options['port'] : '');

        $this->client = $this->__prepareHttpClient();
    }

    /**
     * Prepares the HTTP client
     * @return \Guzzle\Http\Client
     */
    private function __prepareHttpClient()
    {
        $guzzleOption = [
            'request.options' => [
                'verify' => !$this->options['turn_off_ssl_verification'],
                'exceptions' => (isset($this->options['enable_guzzle_exceptions']) && $this->options['enable_guzzle_exceptions'] == true),
                'Content-Type' => 'application/json',
                'query' => ['access_token' => $this->apiAccessToken]
            ],
        ];

        $client = new \Guzzle\Http\Client($this->url, $guzzleOption);
        $client->setUserAgent('wallet/' . $this->version . ';php');

        return $client;
    }


    private function __doGet($url, $params = [])
    {
        $req = $this->client->get($url, null, ['query' => $params]);
        $response = new WalletResponse($req->send());
        return $response->get();
    }

    private function __doPost($url, $postBody)
    {
        $req = $this->client->post($url, null, $postBody);
        $response = new WalletResponse($req->send());
        return $response->get();
    }

    private function __doPut($url, $postBody)
    {
        $req = $this->client->put($url, null, $postBody);
        $response = new WalletResponse($req->send());
        return $response->get();
    }
    
    private function __doDelete($url, $postBody = [])
    {
        $req = $this->client->delete($url, null, $postBody);
        $response = new WalletResponse($req->send());
        return $response->get();
    }
}
